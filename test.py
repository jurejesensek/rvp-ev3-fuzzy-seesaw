from Defuzzer import Defuzzer
from fuzzy.models.Trapezoid import Trapezoid


# input rules for ultrasonic sensor
dist_near = Trapezoid(-float('inf'), -float('inf'), 11, 13)
dist_middle = Trapezoid(11, 13, 18, 20)
dist_far = Trapezoid(18, 20, float('inf'), float('inf'))

# input rules for motor rotation
motor_pos_low = Trapezoid(-2, -1, 76, 86)
motor_pos_middle = Trapezoid(76, 86, 90, 100)
motor_pos_high = Trapezoid(90, 100, 160, 162)

# output rules for motor rotation
motor_lower = Trapezoid(-25, -20, -10, -5)
motor_hold = Trapezoid(-10, -5, 5, 10)
motor_raise = Trapezoid(5, 10, 20, 25)

inputs = [(5, 100),
          (25, 130),
          (5, 50),
          (11, 1),
          (16, 88),
          (25, 50)]
outputs = [-15,
           0,
           0,
           0,
           0,
           15]

outputs.reverse()

for (dist, pos) in inputs:
    # rules
    r_1_1 = dist_near[dist] & motor_pos_low[pos], motor_hold
    r_1_2 = dist_near[dist] & motor_pos_middle[pos], motor_lower
    r_1_3 = dist_near[dist] & motor_pos_high[pos], motor_lower

    r_2_1 = dist_middle[dist] & motor_pos_low[pos], motor_raise
    r_2_2 = dist_middle[dist] & motor_pos_middle[pos], motor_hold
    r_2_3 = dist_middle[dist] & motor_pos_high[pos], motor_lower

    r_3_1 = dist_far[dist] & motor_pos_low[pos], motor_raise
    r_3_2 = dist_far[dist] & motor_pos_middle[pos], motor_raise
    r_3_3 = dist_far[dist] & motor_pos_high[pos], motor_hold

    rules = [r_1_1, r_1_2, r_1_3, r_2_1, r_2_2, r_2_3, r_3_1, r_3_2, r_3_3]
    defuzz = Defuzzer.by_heights(rules)

    pop = outputs.pop()

    print('defuzz: ' + str(defuzz) + ', should be: ' + str(pop))

    assert pop == defuzz
