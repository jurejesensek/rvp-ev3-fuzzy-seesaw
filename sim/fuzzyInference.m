function [zOut] = fuzzyInference(mfX, mfY, mfZ, rules, x, y)

% fuzzification of error velocity, 
% mv: array of membership values: [neg, zero, pos]
mx = zeros(size(mfX,1),1);
for i = 1: size(mfX,1)
    mx(i) = fuzzyTrapeze(mfX(i,:), x);
end

my = zeros(size(mfY,1),1);
for j = 1: size(mfY,1)
    my(j) = fuzzyTrapeze(mfY(j,:), y);
end

% Mamdani, COG
zmin = min(min(mfZ));
zmax = max(max(mfZ));
zRange = zmin: (zmax-zmin)/100: zmax;
% aggregated membership function
m = zeros(size(zRange));
% center of gravity nominator and denominator
COGnom = 0;
COGden = 0;
% loop
for iz = 1: size(zRange,2)
    % fuzzification of z, 
    % mz: array of membership values: [min, norm, max, ...]
    z = zRange(iz);
    m(iz) = 0;
    for i = 1:size(mfX,1)
        for j = 1:size(mfY,1)
            % if AND part
            alphaij = min(mx(i), my(j));
            % Mamdani implication (min)
            mRij = min(alphaij, fuzzyTrapeze(mfZ(rules(i,j),:), z));
            % OR (max) over all rules
            m(iz) = max(m(iz), mRij);
        end
    end    
    % integration (summation)
    COGnom = COGnom + m(iz)*z;
    COGden = COGden + m(iz);
end
zOut = COGnom/COGden;
