function mx = fuzzyTrapeze(pts, x)

    a = pts(1); b = pts(2); c = pts(3); d = pts(4);

    c1 = (x >= a).*(x < b);
    c2 = (x >= b).*(x < c);
    c3 = (x >= c).*(x < d);
    
    r1 = 0
    if (b-a != 0)
      r1 = (x-a)/(b-a)
    endif
    
    r2 = 0
    if (d-c != 0)
      r2 = (x-c)/(d-c)
    endif

    mx = c1.*max(0, min(1, r1)) + c2.*1 + c3.*(1 - max(0, min(1, r2)));

end
