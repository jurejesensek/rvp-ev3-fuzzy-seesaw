% seesaw

clear
clf

% time interval
tRange = 0:0.1:100;

% interesting variables
v = zeros(size(tRange));
a = zeros(size(tRange));
f = zeros(size(tRange));
% initial conditions
v(1) = 0;
a(1) = 0;
f(1) = 0;

% membership functions
% distance
dist = [[-Inf, -Inf, 5, 10]   % near
        [5, 10, 22, 25]       % middle
        [15, 25, Inf, Inf]];  % far

% motor position in
motor = [[-Inf, -Inf, 76, 86]       % down
         [76, 86, 90, 100]      % middle¸
         [90, 100, 160, 162]];  % up
        
% motor position out
out = [[-2, -1, 76, 86]       % down 
       [76, 86, 90, 100]      % middle
       [90, 100, 160, 162]];  % up

% rules   
%   distance:  rows:      1 - near, 2 - middle, 3 - far
%   motor in:  cols:      1 - down, 2 - middle, 3 - up
%   motor out: elements:  1 - down, 2 - middle, 3 - up

rules = [2 1 1; 
         3 2 1; 
         3 3 2];
         
distRange = 3:1:28;
mposRange = 0:3:150;
f = zeros(size(distRange, 2), size(mposRange, 2));     

for ive = 1: size(distRange,2)
    for iver = 1: size(mposRange,2)
        ve = distRange(ive);
        ver = mposRange(iver);
        f(ive,iver) = fuzzyInference(dist, motor, out, rules, ve, ver);
    end
end

surf(mposRange, distRange, f);
xlabel('input motor position [°]', "fontsize", 24);
ylabel('distance [cm]', "fontsize", 24);
zlabel('output motor position [°]', "fontsize", 24);
