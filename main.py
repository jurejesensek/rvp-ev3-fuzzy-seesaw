#!/usr/bin/python3
from ev3dev.ev3 import *
import signal

from Defuzzer import Defuzzer
from fuzzy.models.Trapezoid import Trapezoid

button = TouchSensor('in2')

ultrasonic = UltrasonicSensor('in1')
ultrasonic.mode = 'US-DIST-CM'

motor = Motor('outD')
motor.reset()
motor.stop_action = LargeMotor.STOP_ACTION_BRAKE

ULTRASONIC_MAX_FAR_DIST = 30.0  # 27.3
ULTRASONIC_MIN_NEAR_DIST = 2.0  # 3.1
ULTRASONIC_CENTER_DIST = 15.5  # 15.6

MOTOR_LOWER_LIMIT = 0
MOTOR_RAISE_LIMIT = 150
MOTOR_CENTER = 88


def terminate():
    motor.reset()
    exit(0)


def signal_handler(signum, frame):
    """
        Gracefully stop the robot when pressing ctrl+c in terminal
        :param signum:
        :param frame:
        """
    assert signum == signal.SIGINT
    terminate()


# Attach the signal handler to SIGINT (ctrl+c) signal.
signal.signal(signal.SIGINT, signal_handler)


# input rules for ultrasonic sensor
dist_near = Trapezoid(-float('inf'), -float('inf'), 5, 10)
dist_middle = Trapezoid(5, 10, 22, 25)
dist_far = Trapezoid(15, 25, float('inf'), float('inf'))

# input rules for motor rotation
motor_pos_low = Trapezoid(-float('inf'), -float('inf'), 76, 86)
motor_pos_middle = Trapezoid(76, 86, 90, 100)
motor_pos_high = Trapezoid(90, 100, 160, 162)

# output rules for motor rotation
motor_lower = Trapezoid(-2, -1, 76, 86)
motor_middle = Trapezoid(76, 86, 90, 100)
motor_raise = Trapezoid(90, 100, 160, 162)

# print('dist_near: ' + str(dist_near))
# print('dist_middle: ' + str(dist_middle))
# print('dist_far: ' + str(dist_far))

# print('motor_pos_low: ' + str(motor_pos_low))
# print('motor_pos_middle: ' + str(motor_pos_middle))
# print('motor_pos_high: ' + str(motor_pos_high))


Sound.beep().wait()

last_dist = ULTRASONIC_MAX_FAR_DIST

while not button.is_pressed:
    dist = ultrasonic.distance_centimeters
    if dist > ULTRASONIC_MAX_FAR_DIST or dist < ULTRASONIC_MIN_NEAR_DIST:
        # print('fixed ' + str(dist))
        dist = last_dist
    last_dist = dist

    pos = -motor.position
    # print(str(dist) + " " + str(pos))

    # rules
    r_1_1 = dist_near[dist] & motor_pos_low[pos], motor_middle
    r_1_2 = dist_near[dist] & motor_pos_middle[pos], motor_lower
    r_1_3 = dist_near[dist] & motor_pos_high[pos], motor_lower

    r_2_1 = dist_middle[dist] & motor_pos_low[pos], motor_raise
    r_2_2 = dist_middle[dist] & motor_pos_middle[pos], motor_middle
    r_2_3 = dist_middle[dist] & motor_pos_high[pos], motor_lower

    r_3_1 = dist_far[dist] & motor_pos_low[pos], motor_raise
    r_3_2 = dist_far[dist] & motor_pos_middle[pos], motor_raise
    r_3_3 = dist_far[dist] & motor_pos_high[pos], motor_middle

    list = [r_1_1, r_1_2, r_1_3, r_2_1, r_2_2, r_2_3, r_3_1, r_3_2, r_3_3]
    defuzz_val = Defuzzer.by_heights(list)
    print('%5.2f %6.2f: %6.2f' % (dist, pos, defuzz_val))
    # for a, b in list:
    #     print(str(a.value) + ": " + str(b))
    # print()
    # break

    # don't over extend
    if defuzz_val > 145:
        defuzz_val = 145

    motor.run_to_abs_pos(position_sp=-defuzz_val, speed_sp=100)

terminate()
