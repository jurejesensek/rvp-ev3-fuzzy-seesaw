from fuzzy.Bool import Bool


class TrapezoidCentered:

    def __init__(self, center, w_top, w_ramp):
        """
            |--w_ramp--|--w_top--|         |--w_ramp--|
                       _____________________
                      /          |          \
                    /                         \
                  /              |              \
                /                                 \
              /                  |                  \
        ____/                  center                 \____

        Trapezoid model of a fuzzy set
        :param float center: center value
        :param float w_top: half the top (flat part) width
        :param float w_ramp: width of each of the ramps
        """
        self.center = float(center)
        self.w_top = abs(float(w_top))
        self.w_fall = abs(float(w_ramp))

    def __getitem__(self, value):
        """
        Izračunaj pripadnost množici self
        :param value: vrednost, za katero preverjamo pripadnost množici self
        :return: Bool pripadnost množici self
        """
        offset = value - self.center
        w_full = self.w_top + self.w_fall
        if abs(offset) <= abs(self.w_top):  # on top
            v = 1
        elif abs(offset) > abs(w_full):     # completely off
            v = 0
        elif offset < -self.w_top:     # on rising ramp
            v = (w_full + offset) / self.w_fall
        else:  # if offset > self.w_top:        # on falling ramp
            v = (w_full - offset) / self.w_fall
        assert 0 <= v <= 1
        return Bool(v)

    def __str__(self):
        return 'center=' + str(self.center) + ', w_top=' + str(self.w_top) + ', w_ramp=' + str(self.w_fall)
