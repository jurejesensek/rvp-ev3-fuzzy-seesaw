from fuzzy.Bool import Bool


class Trapezoid:

    def __init__(self, lramp_start, lramp_end, rramp_start, rramp_end):
        """
                lramp_end                   rramp_start
                       |                     |
                       |_____________________|
                      /                      \
                    /                         \
                  /                            \
                /                               \
              /                                  \
        ____/                                     \____
            |                                     |
        lramp_start                            rramp_end

        Trapezoid model of a fuzzy set.
        It can be asymmetrical.
        :param lramp_start: start of rising ramp
        :param lramp_end: end of left ramp and beginning of top
        :param rramp_start: end of top and beginning of falling ramp
        :param rramp_end: end of falling ramp
        """
        self.lramp_start = float(lramp_start)
        self.lramp_end = float(lramp_end)
        self.rramp_start = float(rramp_start)
        self.rramp_end = float(rramp_end)

    def __getitem__(self, value):
        """
        Izračunaj pripadnost množici self
        :param value: vrednost, za katero preverjamo pripadnost množici self
        :return: Bool pripadnost množici self
        """
        if self.lramp_end <= value <= self.rramp_start:             # on top
            v = 1
        elif value < self.lramp_start or value > self.rramp_end:    # completely off
            v = 0
        elif value < self.lramp_end:                                # on rising ramp
            v = (value - self.lramp_start) / (self.lramp_end - self.lramp_start)
        else:  # value > self.rramp.start                           # on falling ramp
            v = (self.rramp_end - value) / (self.rramp_end - self.rramp_start)
        # print('value=' + str(value) + ', v=' + str(v) + ' (' + str(self.lramp_start) + ', ' + str(self.rramp_end) + ')')
        assert 0 <= v <= 1
        return Bool(v)

    def __str__(self):
        return '({})-({})-({})-({})'.format(self.lramp_start, self.lramp_end, self.rramp_start, self.rramp_end)