class Bool:

    def __init__(self, value):
        """
        Fuzzy boolean
        :param value: float pripadnost € [0, 1]
        """
        self.value = float(value)
        assert 0 <= self.value <= 1

    def __float__(self):
        return self.value

    def __bool__(self):
        return bool(self.value)

    def __str__(self):
        return str(self.value)

    def __ior__(self, other):
        assert isinstance(other, Bool)
        self.value = max(self.value, other.value)
        return self

    def __or__(self, other):
        assert isinstance(other, Bool)
        return Bool(max(self.value, other.value))

    def __iand__(self, other):
        assert isinstance(other, Bool)
        self.value = min(self.value, other.value)
        return self

    def __and__(self, other):
        assert isinstance(other, Bool)
        return Bool(min(self.value, other.value))
        # return Bool(self.value if abs(self.value) < abs(other.value) else other.value)

    def __neg__(self):
        return Bool(1.0 - self.value)
