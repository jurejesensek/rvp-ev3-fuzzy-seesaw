from fuzzy.Bool import Bool
from fuzzy.models.Trapezoid import Trapezoid


class Defuzzer:

    @staticmethod
    def by_heights(rules):
        """
        The simplest method
        :param rules: list of tuples (Bool, Trapezoid), where Trapezoid is an output fuzzy set
        :return: final value
        """
        if not rules:
            return None
        numerator = 0
        denominator = 0
        for a, A in rules:
            assert isinstance(a, Bool) and isinstance(A, Trapezoid)
            # todo doesn't work
            numerator += a.value * ((A.lramp_end + A.rramp_start) / 2)
            denominator += a.value
        if denominator != 0:
            return numerator / denominator
        else:
            return 0
