#!/usr/bin/env python3
# so that script can be run from Brickman
import signal

from ev3dev.ev3 import *
import curses

from fuzzy.models.Trapezoid import Trapezoid

button = TouchSensor('in2')

ultrasonic = UltrasonicSensor('in1')
ultrasonic.mode = 'US-DIST-CM'

motor = Motor('outD')
motor.reset()
motor.stop_action = "hold"

ULTRASONIC_MAX_FAR_DIST = 28.0  # 28.3
ULTRASONIC_MIN_NEAR_DIST = 3.0  # 3.1
ULTRASONIC_CENTER_DIST = 15.5  # 15.6

MOTOR_LOWER_LIMIT = 0
MOTOR_RAISE_LIMIT = -140


def terminate():
    motor.reset()
    exit(0)


def signal_handler(signum, frame):
    """
        Gracefully stop the robot when pressing ctrl+c in terminal
        :param signum:
        :param frame:
        """
    assert signum == signal.SIGINT
    terminate()


def interruptor():
    update_screen('')
    # stop goto if space or button pressed
    return screen.getch() == 32 or button.is_pressed


def update_screen(info):
    screen.clear()
    screen.addstr(0, 0, 'Sensor: ' + str(ultrasonic.value()))
    screen.addstr(1, 0, 'Motor: ' + str(motor.position))
    screen.addstr(2, 0, info)
    screen.move(4, 0)


# Attach the signal handler to SIGINT (ctrl+c) signal.
signal.signal(signal.SIGINT, signal_handler)

Sound.beep().wait()

info = ''

try:
    screen = curses.initscr()
    screen.nodelay(True)

    while not button.is_pressed:

        update_screen(info)

        ch = screen.getch()

        # w
        if ch == 119 and motor.position > MOTOR_RAISE_LIMIT:
            motor.run_to_rel_pos(position_sp=-3, speed_sp=600)
            info = 'up'
        # s
        elif ch == 115 and motor.position < MOTOR_LOWER_LIMIT:
            motor.run_to_rel_pos(position_sp=3, speed_sp=600)
            info = 'down'
        else:
            info = 'nothing'

finally:
    curses.endwin()